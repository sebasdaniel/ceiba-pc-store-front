import { by, element } from 'protractor';

export class CurrentOrderPage {

    private mensajeNoComponentesAgregados = element(by.css('app-current-order .alert-heading'));
    private listaComponentes = element.all(by.css('app-current-order table.table tr'));
    private inputNombre = element(by.id('buyerName'));
    private inputNumeroId = element(by.id('buyerIdNumber'));
    private inputEmail = element(by.id('buyerEmail'));
    private inputNumeroTel = element(by.id('buyerPhoneNumber'));
    private inputDireccion = element(by.id('buyerAddress'));
    private inputServicioArmado = element(by.id('buildingService'));

    private campoMensajeSwal = element(by.id('swal2-html-container'));
    private botonOkSwal = element(by.css('button.swal2-confirm'));
    private tituloAlertaSwal = element(by.id('swal2-title'));
    private botonCrearOrden = element(by.css('app-current-order form button.btn-primary'));


    async contarComponentes() {
        return this.listaComponentes.count();
    }

    async ingresarCampoNombre(nombre: string) {
        await this.inputNombre.sendKeys(nombre);
    }

    async ingresarCampoNumeroId(numero: number) {
        await this.inputNumeroId.sendKeys(numero);
    }

    async ingresarCampoEmail(email: string) {
        await this.inputEmail.sendKeys(email);
    }

    async ingresarCampoNumeroTel(tel: string) {
        await this.inputNumeroTel.sendKeys(tel);
    }

    async ingresarCampoDireccion(dir: string) {
        await this.inputDireccion.sendKeys(dir);
    }

    async seleccionarCampoServicioArmado() {
        if (! await this.inputServicioArmado.isSelected()) {
            await this.inputServicioArmado.click();
        }
    }

    async deseleccionarCampoServicioArmado() {
        if (! await this.inputServicioArmado.isSelected()) {
            await this.inputServicioArmado.click();
        }
    }

    async clickBotonCrearOrden() {
        await this.botonCrearOrden.click();
    }

    async obtenerMensajeOrdenVacia() {
        return this.mensajeNoComponentesAgregados.getText();
    }

    async obtenerTituloMensajeAlerta() {
        return this.tituloAlertaSwal.getText();
    }

    async obtenerMensajeAlertDialog() {
        return this.campoMensajeSwal.getText();
    }

    async obtenerCodigoSeguimientoDelAlertDialog() {
        return (await this.campoMensajeSwal.getText()).split('\n')[1];
    }

    async clickBotonOkAlertDialog() {
        await this.botonOkSwal.click();
    }

}
