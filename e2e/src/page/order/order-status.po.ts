import { by, element } from 'protractor';

export class OrderStatusPage {

    private inputCodigoSeguimiento = element(by.id('trackingCode'));
    private botonBuscarOrden = element(by.css('app-order-status button.btn-primary'));
    private mensajeNoResultado = element(by.css('app-order-status .alert-heading'));
    private datosOrden = element.all(by.css('app-order-status section dl dd'));

    async ingresarCampoCodigoSeguimiento(codigo: string) {
        await this.inputCodigoSeguimiento.sendKeys(codigo);
    }

    async clickBotonBuscarOrden() {
        await this.botonBuscarOrden.click();
    }

    async obtenerMensajeNoEncontrado() {
        return this.mensajeNoResultado.getText();
    }

    async obtenerDatoNombre() {
        return this.datosOrden.get(0).getText();
    }

    async obtenerDatoEstado() {
        return this.datosOrden.get(6).getText();
    }

}
