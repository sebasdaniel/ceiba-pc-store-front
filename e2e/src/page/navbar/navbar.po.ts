import { by, element } from 'protractor';

export class NavbarPage {

    linkHome = element(by.xpath('/html/body/app-root/app-navbar/nav/a[1]'));
    linkProducto = element(by.xpath('/html/body/app-root/app-navbar/nav/a[2]'));
    linkComponentes = element(by.xpath('/html/body/app-root/app-navbar/nav/a[2]'));
    linkOrdenActual = element(by.xpath('/html/body/app-root/app-navbar/nav/a[3]'));
    linkConsultarOrden = element(by.xpath('/html/body/app-root/app-navbar/nav/a[4]'));

    async clickBotonProductos() {
        await this.linkProducto.click();
    }

    async clickBotonComponentes() {
        await this.linkComponentes.click();
    }

    async clickBotonOrdenActual() {
        await this.linkOrdenActual.click();
    }

    async clickBotonConsultarOrden() {
        await this.linkConsultarOrden.click();
    }

}
