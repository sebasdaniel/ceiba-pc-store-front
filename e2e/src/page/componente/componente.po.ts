import { by, element } from 'protractor';

export class ComponentePage {

    private listaComponentes = element.all(by.css('table.table tr'));
    private botonesAgregarAOrden = element.all(by.css('table.table tr td button.btn-primary'));
    private botonComponenteAgregado = element(by.css('button.swal2-confirm'));
    private contenedorMensajeComponenteAgregado = element(by.css('#swal2-html-container'));

    async contarComponentes() {
        return this.listaComponentes.count();
    }

    async clickBotonAgregarComponente(index: number) {
        await this.botonesAgregarAOrden.get(index).click();
    }

    async obtenerTextoMensajeComponenteAgregado() {
        return this.contenedorMensajeComponenteAgregado.getText();
    }

    async clickBotonMensajeAgregadoCorrectamente() {
        await this.botonComponenteAgregado.click();
    }

}
