import { NavbarPage } from '../page/navbar/navbar.po';
import { AppPage } from '../app.po';
import { ComponentePage } from '../page/componente/componente.po';

describe('workspace-project PcPart', () => {

    let page: AppPage;
    let navBar: NavbarPage;
    let componente: ComponentePage;

    beforeEach(() => {
        page = new AppPage();
        navBar = new NavbarPage();
        componente = new ComponentePage();
    });

    it('should list pc parts', () => {

        page.navigateTo();
        navBar.clickBotonComponentes();

        expect(componente.contarComponentes()).toBeGreaterThan(0);
    });

    it('should add pc part to order', () => {

        page.navigateTo();
        navBar.clickBotonComponentes();
        componente.clickBotonAgregarComponente(0); // click first button

        expect(componente.obtenerTextoMensajeComponenteAgregado()).toEqual('Componente agregado a la orden');
    });

});
