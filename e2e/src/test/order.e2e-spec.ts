import { NavbarPage } from '../page/navbar/navbar.po';
import { AppPage } from '../app.po';
import { ComponentePage } from '../page/componente/componente.po';
import { CurrentOrderPage } from '../page/order/current-order.po';
import { OrderStatusPage } from '../page/order/order-status.po';
//import { browser } from 'protractor';

describe('workspace-project Order', () => {

    let page: AppPage;
    let navBar: NavbarPage;
    let componente: ComponentePage;
    let currentOrder: CurrentOrderPage;
    let orderStatus: OrderStatusPage;

    beforeEach(() => {
        page = new AppPage();
        navBar = new NavbarPage();
        componente = new ComponentePage();
        currentOrder = new CurrentOrderPage();
        orderStatus = new OrderStatusPage();
    });

    it('should show empty message when enter first time to current order', () => {

        page.navigateTo();
        navBar.clickBotonOrdenActual();

        expect(currentOrder.obtenerMensajeOrdenVacia()).toBe('No ha agregado componentes!');
    });

    // it('should order one component', () => {

    //     // add pc part to order
    //     page.navigateTo();
    //     navBar.clickBotonComponentes();
    //     componente.clickBotonAgregarComponente(0); // click first button
    //     componente.clickBotonMensajeAgregadoCorrectamente(); // dismiss alert

    //     // go to curren order
    //     navBar.clickBotonOrdenActual();

    //     // fill order form
    //     currentOrder.ingresarCampoNombre('Pruebas 32e');
    //     currentOrder.ingresarCampoNumeroId(90123456);
    //     currentOrder.ingresarCampoEmail('e2e@example.com');
    //     currentOrder.ingresarCampoNumeroTel('7891234');
    //     currentOrder.ingresarCampoDireccion('Av siempreviva 123');

    //     // create order
    //     currentOrder.clickBotonCrearOrden();

    //     // varificar que se haya procesado
    //     expect(currentOrder.obtenerTituloMensajeAlerta()).toBe('Orden creada exitosamente!');
    // });

    it('should order and track the ordered component', async () => {

        // add pc part to order
        page.navigateTo();
        navBar.clickBotonComponentes();
        componente.clickBotonAgregarComponente(0); // click first button
        componente.clickBotonMensajeAgregadoCorrectamente(); // dismiss alert

        // go to curren order
        navBar.clickBotonOrdenActual();

        // fill order form
        currentOrder.ingresarCampoNombre('Pruebas 32e');
        currentOrder.ingresarCampoNumeroId(90123456);
        currentOrder.ingresarCampoEmail('e2e@example.com');
        currentOrder.ingresarCampoNumeroTel('7891234');
        currentOrder.ingresarCampoDireccion('Av siempreviva 123');

        // create order
        currentOrder.clickBotonCrearOrden();

        // wait
        // browser.sleep(1000);

        // get tracking code
        let code = await currentOrder.obtenerCodigoSeguimientoDelAlertDialog();

        // click ok button
        currentOrder.clickBotonOkAlertDialog();

        // go to order status (consultar orden)
        navBar.clickBotonConsultarOrden();

        // search by the code
        orderStatus.ingresarCampoCodigoSeguimiento(code);
        orderStatus.clickBotonBuscarOrden();

        // wait
        // browser.sleep(1000);

        // verificar el estado de la orden
        expect(orderStatus.obtenerDatoEstado()).toBe('EN PROCESO');
    });

    it('should order all components plus building service', async () => {

        // add pc part to order
        page.navigateTo();
        navBar.clickBotonComponentes();
        componente.clickBotonAgregarComponente(0); // click button 1
        componente.clickBotonMensajeAgregadoCorrectamente(); // dismiss alert
        componente.clickBotonAgregarComponente(1); // click button 2
        componente.clickBotonMensajeAgregadoCorrectamente();
        componente.clickBotonAgregarComponente(2); // click button 3
        componente.clickBotonMensajeAgregadoCorrectamente();
        componente.clickBotonAgregarComponente(3); // click button 4
        componente.clickBotonMensajeAgregadoCorrectamente();
        componente.clickBotonAgregarComponente(4); // click button 5
        componente.clickBotonMensajeAgregadoCorrectamente();
        componente.clickBotonAgregarComponente(5); // click button 6
        componente.clickBotonMensajeAgregadoCorrectamente();
        componente.clickBotonAgregarComponente(6); // click button 7
        componente.clickBotonMensajeAgregadoCorrectamente();

        // go to curren order
        navBar.clickBotonOrdenActual();

        // fill order form
        currentOrder.ingresarCampoNombre('Pruebas 32e');
        currentOrder.ingresarCampoNumeroId(90123456);
        currentOrder.ingresarCampoEmail('e2e@example.com');
        currentOrder.ingresarCampoNumeroTel('7891234');
        currentOrder.ingresarCampoDireccion('Av siempreviva 123');
        currentOrder.seleccionarCampoServicioArmado();

        //browser.sleep(5000);

        // create order
        currentOrder.clickBotonCrearOrden();

        // obtener titulo
        let titulo = await currentOrder.obtenerTituloMensajeAlerta();

        // varificar que se haya procesado
        expect(titulo).toBe('Orden creada exitosamente!');
    });

});
