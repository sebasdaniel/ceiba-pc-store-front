import { Injectable } from '@angular/core';

import Swal from 'sweetalert2';

const CONFIRM_BUTTON_COLOR = '#0069d9';

@Injectable({
  providedIn: 'root'
})
export class AlertMessageService {

  constructor() { }

  public showInfoMessage(msg: string, customTitle?: string) {
    Swal.fire({
      title: customTitle,
      html: msg,
      icon: 'info',
      confirmButtonColor: CONFIRM_BUTTON_COLOR
    });
  }

  public showSuccessMessage(msg: string, customTitle?: string) {
    Swal.fire({
      title: customTitle,
      html: msg,
      icon: 'success',
      confirmButtonColor: CONFIRM_BUTTON_COLOR
    });
  }

  public showErrorMessage(msg: string, customTitle?: string) {
    Swal.fire({
      title: customTitle,
      html: msg,
      icon: 'error',
      confirmButtonColor: CONFIRM_BUTTON_COLOR
    });
  }

  public showWarningMessage(msg: string, customTitle?: string) {
    Swal.fire({
      title: customTitle,
      html: msg,
      icon: 'warning',
      confirmButtonColor: CONFIRM_BUTTON_COLOR
    });
  }

  public showSimpleMessage(msg: string, customTitle?: string) {
    Swal.fire({
      title: customTitle,
      html: msg,
      confirmButtonColor: CONFIRM_BUTTON_COLOR
    });
  }

}
