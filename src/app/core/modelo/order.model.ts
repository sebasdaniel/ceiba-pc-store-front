import { PcPart } from './pc-part.model';

export class Order {

    id: number;
    buildService: boolean;
    placementDate: string;
    shippingDate: string;
    deliveredDate: string;
    status: string;
    trackingCode: string;
    buyerName: string;
    buyerIdNumber: number;
    buyerEmail: string;
    buyerPhoneNumber: string;
    buyerAddress: string;
    orderComponents: PcPart[];
    orderPrice: number;

}
