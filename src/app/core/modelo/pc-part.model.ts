export class PcPart {

    id: number;
    name: string;
    type: string;
    price: number;

}
