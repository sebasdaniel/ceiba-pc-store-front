import { Injectable } from '@angular/core';
import { Order } from '@core/modelo/order.model';
import { PcPart } from '@core/modelo/pc-part.model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderCartService {

  private orderCart = new Order();
  private orderSource = new BehaviorSubject(this.orderCart);
  currentOrder = this.orderSource.asObservable();

  constructor() { }

  /**
   * Reset the order as new order (empty order)
   */
  resetOrder() {
    this.orderCart = new Order();
    this.orderSource.next(this.orderCart);
  }

  /**
   * Add a PcPart object to the orderComponents array.
   *
   * @param pcPart the PcPart object to add
   */
  addPcPart(pcPart: PcPart) {

    if (this.orderCart.orderComponents === undefined) {
      this.orderCart.orderComponents = new Array();
    }

    this.orderCart.orderComponents.push(pcPart);
  }

  /**
   * Delete a PcPart object from the orderComponents array.
   *
   * @param pcPart th PcPart object to delete
   */
  dropPcPart(pcPart: PcPart) {

    // if there are components in the array
    if (this.orderCart.orderComponents != null && this.orderCart.orderComponents.length > 0) {

      // get the index of the component to drop and create a new array without it
      const index = this.orderCart.orderComponents.indexOf(pcPart);

      if (index > -1) {
        this.orderCart.orderComponents.splice(index, 1);
      }
    }

    // of all component was dopped
    if (this.orderCart.orderComponents != null && this.orderCart.orderComponents.length === 0) {
      this.orderCart.orderComponents = null;
    }
  }

}
