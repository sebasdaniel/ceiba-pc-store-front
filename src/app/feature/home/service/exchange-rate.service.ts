import { Injectable } from '@angular/core';
import { HttpService } from '@core-service/http.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExchangeRateService {

  constructor(protected http: HttpService) { }

  public callExchangeRateService() {

    const url = `${environment.exchangeServiceUrl}/latest.json?app_id=${environment.exchangeServiceId}`;
    return this.http.doGet<any>(url);
  }
}
