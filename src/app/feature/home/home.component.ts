import { Component, OnInit } from '@angular/core';
import { ExchangeRateService } from './service/exchange-rate.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  // styleUrls: ['./home.component.']
})
export class HomeComponent implements OnInit {

  public trm: string;

  constructor(protected exchangeService: ExchangeRateService) { }

  ngOnInit() {

    // currecny formatter
    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      maximumFractionDigits: 2
    });

    // call service and set value
    this.exchangeService.callExchangeRateService().subscribe((resp: any) => {
      // this.trm = `${resp.rates.COP}`.substr(0,7);
      this.trm = formatter.format(resp.rates.COP);
    });
  }

}
