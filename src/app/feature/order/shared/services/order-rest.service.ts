import { Injectable } from '@angular/core';
import { Order } from '@core/modelo/order.model';
import { HttpService } from '@core/services/http.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class OrderRestService {

  constructor(protected httpService: HttpService) { }

  /**
   * Call backend service to save a new order.
   *
   * @param order the Order to save
   * @returns     Observable<Order>
   */
   public createOrder(order: Order) {
    return this.httpService.doPost<Order, Order>(`${environment.backendServiceUrl}/orders`, order);
  }

  /**
   * Call backend service to get an order by its tracking code.
   *
   * @param trackingCode the tracking code
   * @returns            Observable<Order>
   */
  public getOrder(trackingCode: string) {
    return this.httpService.doGet<Order>(`${environment.backendServiceUrl}/orders/${trackingCode}`);
  }

}
