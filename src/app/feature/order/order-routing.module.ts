import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CurrentOrderComponent } from './current-order/current-order.component';
import { OrderStatusComponent } from './order-status/order-status.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'estado', component: OrderStatusComponent },
      { path: 'actual', component: CurrentOrderComponent }
    ]
  }
  // { path: '', redirectTo: '/orden/actual', pathMatch: 'full' },
  // { path: 'orden/estado', component: OrderStatusComponent },
  // { path: 'orden/actual', component: CurrentOrderComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
