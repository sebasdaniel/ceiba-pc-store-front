import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderRoutingModule } from './order-routing.module';
import { CurrentOrderComponent } from './current-order/current-order.component';
import { OrderStatusComponent } from './order-status/order-status.component';
import { OrderRestService } from './shared/services/order-rest.service';
// import { SharedModule } from '@shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


@NgModule({
  declarations: [
    CurrentOrderComponent,
    OrderStatusComponent
  ],
  imports: [
    CommonModule,
    OrderRoutingModule,
    // SharedModule
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  providers: [OrderRestService]
})
export class OrderModule { }
