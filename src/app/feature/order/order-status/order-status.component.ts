import { Component, OnInit } from '@angular/core';
import { Order } from '@core/modelo/order.model';
import { OrderRestService } from '../shared/services/order-rest.service';

@Component({
  selector: 'app-order-status',
  templateUrl: './order-status.component.html'
})
export class OrderStatusComponent implements OnInit {

  public myOrder: Order;

  constructor(protected restService: OrderRestService) { }

  ngOnInit(): void {
  }

  public findOrder(trackingCode: string) {

    // valid tracking code
    if (trackingCode === null || trackingCode === undefined || trackingCode.length === 0) {
      alert('Ingrese un codigo de seguimiento');
    } else {

      // call the service and get the order
      this.restService.getOrder(trackingCode).subscribe((resp: any) => {

        this.myOrder = resp;
        console.log('called findOrder:');
        console.log(resp);

      }, (err) => {

        if (err.status === 404) {
          this.myOrder = null;
        }
        console.log('findOrder error:');
        console.log(err);
      });
    }
  }

  public convertStatusToReadable(status: string) {

    switch (status) {
      case 'PROCESSING': {
        return 'EN PROCESO';
      }
      case 'SHIPPED': {
        return 'ENVIADA';
      }
      case 'DELIVERED': {
        return 'ENTREGADA';
      }
    }

    return 'Unknow status';
  }

}
