import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Order } from '@core/modelo/order.model';
import { OrderCartService } from '@shared/services/order-cart.service';
import { Subscription } from 'rxjs';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { PcPart } from '@core/modelo/pc-part.model';
import { AlertMessageService } from '@core/services/alert-message.service';
import { formatCurrency } from '@angular/common';
import { OrderRestService } from '../shared/services/order-rest.service';


@Component({
  selector: 'app-current-order',
  templateUrl: './current-order.component.html'
})
export class CurrentOrderComponent implements OnInit, OnDestroy {

  orderForm: FormGroup;
  currentOrder: Order;
  subscription: Subscription;
  faTrash = faTrash;

  constructor(
      protected orderCart: OrderCartService,
      protected restService: OrderRestService,
      protected alertServie: AlertMessageService
  ) { }

  ngOnInit(): void {
    this.subscription = this.orderCart.currentOrder.subscribe(order => this.currentOrder = order);
    this.buildOrderForm();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  /**
   * Build the form with angular reactive forms
   */
  private buildOrderForm() {
    this.orderForm = new FormGroup({
      buyerName: new FormControl('', [Validators.required]),
      buyerIdNumber: new FormControl('', [Validators.required]),
      buyerEmail: new FormControl(''),
      buyerPhoneNumber: new FormControl('', [Validators.required]),
      buyerAddress: new FormControl('', [Validators.required]),
      buildService: new FormControl(false)
    });
  }

  /**
   * Set the form elements values into the currentOrder fields.
   */
  private orderFormToCurrentOrder() {
    this.currentOrder.buyerName = this.orderForm.value.buyerName;
    this.currentOrder.buyerIdNumber = this.orderForm.value.buyerIdNumber;
    this.currentOrder.buyerEmail = this.orderForm.value.buyerEmail;
    this.currentOrder.buyerPhoneNumber = this.orderForm.value.buyerPhoneNumber;
    this.currentOrder.buyerAddress = this.orderForm.value.buyerAddress;
    this.currentOrder.buildService = this.orderForm.value.buildService;
  }

  public submitOrder() {

    // valid form
    if (this.orderForm.valid) {

      // process the form
      this.orderFormToCurrentOrder();

      // call service to create a new order
      this.restService.createOrder(this.currentOrder).subscribe((resp: any) => {

        // get order result
        const orderResult: Order = resp.valor;

        // show successful message with the tracking code and final price
        const message = 'El c&oacute;digo de seguimiento es:<br>' + orderResult.trackingCode
        + '<br><br>EL valor total es de ' + formatCurrency(orderResult.orderPrice, 'en-US', '$');
        this.alertServie.showSuccessMessage(message, 'Orden creada exitosamente!');

        // reset order
        this.orderCart.resetOrder();

      }, (err) => {

        if (err.status === 400) {
          this.alertServie.showErrorMessage(err.error.mensaje);
        }

        console.log('Error al crear orden:');
        console.log(err);
      });
    } else {
      this.alertServie.showErrorMessage('Todos los campos marcados con (<b>*</b>) son obligatorios');
    }
  }

  /**
   * Drop a component from the current order.
   *
   * @param pcPart the component to drop from the order
   */
  public deleteFromCart(pcPart: PcPart) {
    this.orderCart.dropPcPart(pcPart);
  }

  /**
   * Return the total price of the order components.
   *
   * @returns the order components total price
   */
  public getOrderTotalPrice() {

    let totalPrice = 0;

    this.currentOrder.orderComponents.forEach((comp) => {
      totalPrice += comp.price;
    });

    return totalPrice;
  }

}
