import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { PcpartListComponent } from '@pcpart-list/pcpart-list.component';
import { PcpartListComponent } from './pcpart-list/pcpart-list.component';

const routes: Routes = [{ path: '', component: PcpartListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PcpartRoutingModule { }
