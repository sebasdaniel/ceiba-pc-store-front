import { TestBed } from '@angular/core/testing';

import { PcPartRestService } from './pc-part-rest.service';

describe('PcPartRestServiceService', () => {
  let service: PcPartRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PcPartRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
