import { Injectable } from '@angular/core';
import { PcPart } from '@core/modelo/pc-part.model';
import { HttpService } from '@core/services/http.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class PcPartRestService {

  constructor(protected httpService: HttpService) { }

  /**
   * Call the service to get the component list.
   *
   * @returns Observable<PcPart[]>
   */
   public getPcPartList() {
    return this.httpService.doGet<PcPart[]>(`${environment.backendServiceUrl}/components`);
  }

}
