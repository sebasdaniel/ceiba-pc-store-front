import { Component, OnInit } from '@angular/core';

import { PcPart } from '@core/modelo/pc-part.model';
import { AlertMessageService } from '@core/services/alert-message.service';
import { OrderCartService } from '@shared/services/order-cart.service';
import { faCartPlus } from '@fortawesome/free-solid-svg-icons';
import { PcPartRestService } from '../shared/services/pc-part-rest.service';

@Component({
  selector: 'app-pcpart-list',
  templateUrl: './pcpart-list.component.html'
})
export class PcpartListComponent implements OnInit {

  public pcPartList: PcPart[];
  //  = [
  //   { id: 1, name: "B550", type: "Board", price: 123.4},
  //   { id: 2, name: "R3600", type: "CPU", price: 123.4},
  //   { id: 3, name: "RTX 3080", type: "Graphic Card", price: 1203.2}
  // ];

  faCart = faCartPlus;

  constructor(
      protected restService: PcPartRestService,
      protected cartService: OrderCartService,
      protected alertService: AlertMessageService
  ) { }

  ngOnInit(): void {

    // call the rest service and get the PcPart list
    this.restService.getPcPartList().subscribe((resp: any) => {
      this.pcPartList = resp;
    });
  }

  public addToCart(pcPart: PcPart) {

    // console.log(pcPart); // TODO: mostrar toast message
    this.cartService.addPcPart(pcPart);
    // alert('Agregado correctamente');
    this.alertService.showSuccessMessage('Componente agregado a la orden');
  }

}
