import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PcpartRoutingModule } from './pcpart-routing.module';
// import { PcpartListComponent } from '@pcpart-list/pcpart-list.component';
import { PcpartListComponent } from './pcpart-list/pcpart-list.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PcPartRestService } from './shared/services/pc-part-rest.service';


@NgModule({
  declarations: [
    PcpartListComponent
  ],
  imports: [
    CommonModule,
    PcpartRoutingModule,
    FontAwesomeModule
  ],
  providers: [PcPartRestService]
})
export class PcpartModule { }
